//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by WinLotto.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_TREEPROPSHEETEX_DEMO_DIALOG 102
#define IDS_STRING_PROJECT              102
#define IDD_PAGE_CONTACT                107
#define IDD_PAGE_CONTACT_EMAIL          108
#define IDD_PAGE_CONTANCT_PHONE         109
#define IDD_PAGE_NOTE                   110
#define IDD_PAGE_MISC_NOTE              110
#define IDD_PAGE_CUSTOMIZE_SHEET        111
#define IDD_PAGE_MISC_DATES             112
#define IDR_MAINFRAME                   128
#define IDI_MAINFRAME                   128
#define IDI_MAIL                        135
#define IDI_TELEPHONE                   136
#define IDI_NOTE                        137
#define IDI_CONTACT                     138
#define IDB_EMPTY_IMAGE_LIST            140
#define IDI_DATE_TIME                   141
#define IDD_DLG_CHILD_SHEETS            142
#define IDI_PREFERENCES                 143
#define IDD_PAGE_WINS                   146
#define IDD_PAGE_MISC_DATES1            147
#define IDD_PAGE_EMPTY                  147
#define IDC_RESIZABLE_TREEPROPSHEETEX   1000
#define IDC_CLASSIC_TREEPROPSHEET       1001
#define IDC_RESIZABLE_TABBED_TREEPROPSHEETEX 1002
#define IDC_RESIZABLE_WIZARD_TREEPROPSHEETEX 1003
#define IDC_LABEL_ADDRESS               1004
#define IDC_CHILD_TREEPROPSHEETEX       1004
#define IDC_LABEL_NAME                  1005
#define IDC_NAME                        1006
#define IDC_ADDRESS                     1007
#define IDC_EMAIL1                      1008
#define IDC_EMAIL2                      1009
#define IDC_EMAIL3                      1010
#define IDC_COMBO_DEFAULT_EMAIL         1011
#define IDC_PHONE1                      1012
#define IDC_PHONE2                      1013
#define IDC_PHONE3                      1014
#define IDC_COMBO_DEFAULT_PHONE         1015
#define IDC_NOTE                        1016
#define IDC_RESIZABLE_PROPERTYSHEET     1017
#define IDC_FIXED                       1018
#define IDC_PROPORTIONAL                1019
#define IDC_SKIP_EMPTY_PAGES            1022
#define IDC_REAL_TIME_REFRESH_MODE      1023
#define IDC_RESIZING_FRAME              1025
#define IDC_REFRESH_FRAME               1026
#define IDC_EMPTY_PAGES_FRAME           1027
#define IDC_CHK_TREE_RESIZABLE          1028
#define IDC_DATE_BIRTHDAY               1029
#define IDC_DATE_ANNIVERSARY            1030
#define IDC_MODELESS                    1031
#define IDC_TOP_SHEET                   1032
#define IDC_BOTTOM_SHEET                1033
#define IDC_TREEPROPSHEETOFFICE2003     1034
#define IDC_SLIDER1                     1035
#define IDC_CHECK_PHONE                 1037
#define IDC_CHECK_EMAIL                 1038
#define IDC_LIST_WINS                   1039

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        147
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1040
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
